package com.okey.primerLogins.ui.home;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.okey.primerLogins.MeQuery;
import com.okey.primerLogins.R;
import com.okey.primerLogins.RequestTokenInterceptor;

import org.jetbrains.annotations.NotNull;

import okhttp3.OkHttpClient;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;
    private static String TAG = "RNLOGINS";
    private static final String BASE_GRAPHQL_SERVER_URL = "http://10.0.2.2:4000";

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        homeViewModel =
                ViewModelProviders.of(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        final TextView textView = root.findViewById(R.id.text_home);
        homeViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });

        Bundle arguments = this.getArguments();
        if( arguments != null ) {

            String token = arguments.getString("token");
            OkHttpClient okHttpClient = new OkHttpClient.Builder()
                    .addInterceptor(new RequestTokenInterceptor(token))
                    .build();
            ApolloClient apolloClient = ApolloClient.builder()
                    .serverUrl(BASE_GRAPHQL_SERVER_URL)
                    .okHttpClient(okHttpClient)
                    .build();
            MeQuery meQuery = MeQuery.builder()
                    .first(3)
                    .after("")
                    .build();
            apolloClient.query(meQuery)
                    .enqueue(new ApolloCall.Callback<MeQuery.Data>() {

                        @Override
                        public void onResponse(@NotNull Response<MeQuery.Data> dataResponse) {
                            Log.i(TAG, dataResponse.toString());
                        }

                        @Override
                        public void onFailure(@NotNull ApolloException e) {
                            Log.e(TAG, e.getMessage(), e);
                        }
                    });
        }

        return root;
    }
}