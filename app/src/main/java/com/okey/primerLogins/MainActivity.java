package com.okey.primerLogins;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Response;
import com.apollographql.apollo.exception.ApolloException;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.okey.primerLogins.models.User;
import com.okey.primerLogins.type.UserInput;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import okhttp3.OkHttpClient;

public class MainActivity extends AppCompatActivity {

    LoginButton fbLoginButton;
    CallbackManager fbCallbackManager;
    ProfileTracker fbProfileTracker;
    AccessToken fbToken;

    final String TAG = "RNLOGINS";

    private static final String BASE_GRAPHQL_SERVER_URL = "http://10.0.2.2:4000";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        AccessToken accessToken = AccessToken.getCurrentAccessToken();
        boolean isLoggedIn = accessToken != null && !accessToken.isExpired();

        fbLoginButton = (LoginButton) findViewById(R.id.fb_login_button);
        Log.d(TAG, "FB Auth type: " + fbLoginButton.getAuthType());

        fbCallbackManager = CallbackManager.Factory.create();
        fbLoginButton.registerCallback(fbCallbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        fbToken = loginResult.getAccessToken();

                        GraphRequest request = GraphRequest.newMeRequest( loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

                            @Override
                            public void onCompleted(JSONObject me, GraphResponse response) {

                                String email = me.optString("email");
                                String id = me.optString("id");
                                String name = me.optString("name");
                                JSONObject picture = me.optJSONObject("picture");
                                JSONObject pictureData = picture.optJSONObject("data");
                                String pictureUrl = pictureData.optString("url");

                                // GraphQL type
                                UserInput user = UserInput.builder()
                                        .name(name)
                                        .email(email)
                                        .pictureUrl(pictureUrl)
                                        .token(fbToken.getToken())
                                        .providerName("facebook")
                                        .build();

                                registerUser(user);
                            }
                        });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "name,email,picture");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });

    }

    private boolean registerUser(final UserInput user) {

        OkHttpClient okHttpClient = new OkHttpClient.Builder().build();
        ApolloClient apolloClient = ApolloClient.builder()
                .serverUrl(BASE_GRAPHQL_SERVER_URL)
                .okHttpClient(okHttpClient)
                .build();

        RegisterMutation registerMutation = RegisterMutation.builder()
                .user(user)
                .build();

        final Activity self = this;
        apolloClient.mutate(registerMutation)
        .enqueue(new ApolloCall.Callback<RegisterMutation.Data>() {
            @Override
            public void onResponse(@NotNull Response<RegisterMutation.Data> response) {
                if( !response.hasErrors() ) {
                    if (response.data().register) {
                        Log.d(TAG, "registered");

                        Intent intent = new Intent(self, HomeActivity.class);
                        User parcellableUser = new User(user.name(), user.email(), user.pictureUrl(), user.token());
                        intent.putExtra("userTag", parcellableUser);
                        startActivity(intent);
                    }
                }
            }

            @Override
            public void onFailure(@NotNull ApolloException e) {
                Log.e(TAG, e.getMessage());
            }
        });

        return true;

    }

    private void updateUI(Profile profile) {
        if( profile == null )
            return;

//        txtUserName.setText(profile.getName());
//        Uri uri  = Uri.parse("http://graph.facebook.com/" + profile.getId() + "/picture?type=large");
//        setUserPicture(uri);
    }

    private void restoreUI() {
//        txtUserName.setText(R.string.prompt);
//        userImageView.setImageDrawable(null);
//
//        signInButton.setVisibility(View.VISIBLE);
//        btnSignOut.setVisibility(View.GONE);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        fbCallbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
