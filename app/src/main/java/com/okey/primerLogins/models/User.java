package com.okey.primerLogins.models;

import android.os.Parcel;
import android.os.Parcelable;

public class User implements Parcelable {

    private String name;
    private String email;
    private String pictureUrl;
    private String token;

    public String name() {
        return name;
    }

    public String email() {
        return email;
    }

    public String pictureUrl() {
        return  pictureUrl;
    }

    public String token() {
        return token;
    }

    public User(String name,String email,String pictureUrl, String token){
        this.name = name;
        this.email = email;
        this.pictureUrl = pictureUrl;
        this.token = token;
    }

    // Parcel stuff
    public User(Parcel in) {
        String[] data = new String[4];

        in.readStringArray(data);
        this.name = data[0];
        this.email = data[1];
        this.pictureUrl= data[2];
        this.token = data[3];
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringArray(new String[]{
                this.name,
                this.email,
                this.pictureUrl,
                this.token
        });
    }

    public static final Parcelable.Creator<User> CREATOR= new Parcelable.Creator<User>() {

        @Override
        public User createFromParcel(Parcel source) {
        // TODO Auto-generated method stub
            return new User(source);  //using parcelable constructor
        }

        @Override
        public User[] newArray(int size) {
        // TODO Auto-generated method stub
            return new User[size];
        }
    };

}
